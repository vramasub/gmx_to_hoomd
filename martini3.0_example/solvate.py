from hoomd import *

import numpy as np
from scipy import spatial
import math

def solvate(system,rho_water=None,rho_ions=None, vdw_cut=0.21):
    solvent_tags = []
    tree_pos = []

    box = system.box
    lvec = np.array([box.get_lattice_vector(0),box.get_lattice_vector(1),box.get_lattice_vector(2)])
    rvec = np.array([np.cross(lvec[1],lvec[2]), np.cross(lvec[2],lvec[0]), np.cross(lvec[0],lvec[1])])
    rvec /= box.get_volume()
    nearest_plane_distance = np.array([1/np.linalg.norm(rvec[0]),1/np.linalg.norm(rvec[1]), 1/np.linalg.norm(rvec[2])])

    # MARTINI3 water
    water_mass = 72
    water_type = 'WN'
    water_charge = 0
    rho_water = 33.4556/4.0
    a_water = math.pow(rho_water,-1./3.)

    # Sodium
    Na_mass = 36
    Na_type = 'TQ1'
    Na_charge = 1

    # Chloride
    Cl_mass = 36
    Cl_type = 'TQ1'
    Cl_charge = -1

    N_Na = N_Cl = 0

    # add water type
    system.particles.types.add(water_type)

    (a,b,c) = nearest_plane_distance
    (lx,ly,lz) = (int(a/a_water),int(b/a_water),int(c/a_water))
    nwater = 0
    nwater_max = int(lx)*int(ly)*int(lz)

    if rho_ions is not None:
        # add bead types
        system.particles.types.add(Na_type)
        system.particles.types.add(Cl_type)

    snap = system.take_snapshot()

    if rho_ions is not None:
        # get total system charge
        total_charge = np.sum(snap.particles.charge)
        N_Na = N_Cl = int(round((rho_ions*box.get_volume())))
        if total_charge < 0:
            N_Na += int(round(total_charge/Cl_charge))
        else:
            N_Cl += int(round(total_charge/Na_charge))

    context.msg.notice(1,'N_Na = {}\n'.format(N_Na))
    context.msg.notice(1,'N_Cl = {}\n'.format(N_Cl))

    if comm.get_rank() == 0:
        context.msg.notice(1,"Storing {} particles in tree\n".format(len(snap.particles.position)))
        # store particles in tree
        f_vdw = vdw_cut/nearest_plane_distance
        for pos in snap.particles.position:
            f = box.make_fraction(pos)
            for i in range(-1,2):
                if i==-1:
                    if f[0] < 1.0-f_vdw[0]: continue
                if i==1:
                    if f[0] > f_vdw[0]: continue
                for j in range(-1,2):
                    if j==-1:
                        if f[1] < 1.0-f_vdw[1]: continue
                    if j==1:
                        if f[1] > f_vdw[1]: continue
                    for k in range(-1,2):
                        if k==-1:
                            if f[2] < 1.0-f_vdw[2]: continue
                        if k==1:
                            if f[2] > f_vdw[2]: continue
                        tree_pos.append(pos+np.dot((i,j,k),lvec))

        tree = spatial.cKDTree(tree_pos)

        new_size = old_size = snap.particles.N
        snap.particles.resize(new_size+nwater_max)

        context.msg.notice(1,"Trying to add {} molecules\n".format(nwater_max))
        for i in range(int(lx)):
            for j in range(int(ly)):
                for k in range(int(lz)):
                    fx = -0.5+i/lx
                    fy = -0.5+j/ly
                    fz = -0.5+k/lz
                    p = fx*lvec[0]+fy*lvec[1]+fz*lvec[2]

                    neighbors = tree.query_ball_point(p,vdw_cut)
                    if len(neighbors) == 0:
                        mytype = water_type
                        snap.particles.position[new_size] = p
                        snap.particles.mass[new_size] = water_mass
                        snap.particles.typeid[new_size] = snap.particles.types.index(mytype)
                        snap.particles.charge[new_size] = water_charge
                        solvent_tags.append(new_size)
                        new_size+=1
                        nwater += 1

                        if (nwater % 100000) == 0:
                            context.msg.notice(1,"{}th molecule\n".format(nwater))

        # Turn random water beads into ions
        import random
        waters = list(range(old_size,old_size+nwater))
        ion_beads = random.sample(waters, N_Na+N_Cl)

        for i,ibead in enumerate(ion_beads):
            if i < N_Na:
                snap.particles.mass[ibead] = Na_mass
                snap.particles.typeid[ibead] = snap.particles.types.index(Na_type)
                snap.particles.charge[ibead] = Na_charge
            else:
                snap.particles.mass[ibead] = Cl_mass
                snap.particles.typeid[ibead] = snap.particles.types.index(Cl_type)
                snap.particles.charge[ibead] = Cl_charge

        # set actual size
        snap.particles.resize(new_size)

    system.restore_snapshot(snap)
    return solvent_tags

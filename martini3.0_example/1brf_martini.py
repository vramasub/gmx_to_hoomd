from hoomd import *
from hoomd import md

import math

context.initialize()

import ff_1brf_cg30 as ff

from hoomd import deprecated

# Bead type colors from Montecelli et al. JCTC 2008
colors = {'C': (64,76,138), 'N': (2,150,81), 'P': (254,240,81), 'Q': (200, 66, 60)}
colors['S'] = colors['T'] = (2,150,81) # Martini 3 small and tiny
colors['W'] = (0x59,0x84,0xff)

system = deprecated.init.read_xml('1brf_cg30.xml')

# add water beads and ions
M = 3.0 # mol/L
N_Avogadro = 6.022e23
rho_ions = M*N_Avogadro*1e-24 # per nm^3

import solvate
solvate.solvate(system,rho_ions=rho_ions)

nl = md.nlist.cell()

rcut = 1.1
lj = md.pair.lj(r_cut=rcut,nlist=nl)

pair_coeff = ff.nonbonded_coeff()
util.quiet_status()
for (ti,tj) in pair_coeff.values:
    lj.pair_coeff.set(ti,tj,sigma=pair_coeff.get(ti,tj,'sigma'),epsilon=pair_coeff.get(ti,tj,'epsilon'))
util.unquiet_status()

ff.intrachain(nl,nb_cut=rcut)

# units
T_kelvin = 300
P_bar = 1.0
reduced_T_unit = 0.008314510
T = T_kelvin*reduced_T_unit
reduced_P_unit = 0.0602214
P = P_bar*reduced_P_unit

# rescale charges
l_bjerrum = 0.7*T
for p in system.particles:
    p.charge *= math.sqrt(l_bjerrum)

# replicate system twice along every dimension
system.replicate(nx=2,ny=2,nz=2)

# charged interactions, need to choose appropriate Nx,Ny,Nz
pppm = md.charge.pppm(group=group.all(), nlist=nl)
pppm.set_params(Nx=64,Ny=64,Nz=64,order=5,rcut=0.8)

# warm up
integrator = md.integrate.mode_standard(dt=0.001)
nve = md.integrate.nve(group=group.all(),limit=0.01)
run(1000)
nve.disable()

#nvt = md.integrate.nvt(tau=1.0,kT=T,group=group.all())
npt = md.integrate.npt(tau=1.0,tauP=1.0,kT=T, P=P, group=group.all())

run(25000)

integrator.set_params(dt=0.025)

# setup pos writer
pos = deprecated.dump.pos(filename='out.pos',period=10000)
for t in system.particles.types:
    if t[0] in colors:
        (r,g,b) = colors[t[0]]
    else:
        (r,g,b) = (255,255,255)

    if t == 'TQ1':
        # ion
        (r,g,b) = (200,200,200)
    color = '%02x%02x%02x%02x' %  (255,r,g,b)
    pos.set_def(t,'sphere {:.3f} {}'.format(ff.diameter[t], color))

run(1e5)

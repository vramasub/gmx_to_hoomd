# read_gromacs.py
# Utility script to convert a gromacs topology to a HOOMD input script
#
# NOTE: The force field generated is topology-specific, i.e.
# this script needs to be called once for every different .pdb input file
#
from gromacs import *
from pdb import *

from hoomd import *
import numpy as np

# epsilon_r (dielectric coefficient)
#epsilon = 15 # with MARTINI water
epsilon = 1

water_type = 'P4'

# attractive interaction scaling parameter, see Stark, Andrew, Elcock JCTC 2013 9, 4176-4185
alpha=1.0

if len(sys.argv) < 4:
    print('Syntax: python read_gromacs.py input.pdb input.top output.xml [output.py]')
    sys.exit(1)

pdb_file = sys.argv[1]
top_file = sys.argv[2]
xml_file = sys.argv[3]

ff_file = None
if len(sys.argv) > 4:
    ff_file = sys.argv[4]

pdb = pdb()
pdb.read(pdb_file)
gro = gromacs()
gro.read_topology(top_file)

print('#Writing XML...')

xml_pos = []
xml_type = []
xml_mass = []
xml_charge = []
xml_image = []
xml_diameter = []
xml_bond = []
xml_bond_type = []
xml_pair = []
xml_pair_type = []
xml_angle = []
xml_angle_type = []
xml_dihedral = []
xml_dihedral_type = []
xml_improper = []
xml_improper_type = []
xml_velocity = []
xml_body = []
xml_constraint = []

body_pos = dict()
body_type = dict()
body_res = dict()
body_resid = dict()
body_charge = dict()
body_mass = dict()
body_total_mass = dict()
body_bfact = dict()

backbone = []
groups = dict()
chain_offset = 0

chain_pos = dict()
chain_type =dict()

nr_excl = 0

imol = 0
cur_body = 0

has_virtual_sites = False

atom_idx = 0
for (chain_id, chain_atoms) in zip(gro.chains,pdb.chain_atoms):
    nmol = len(chain_atoms)

    lidx = 0
    while nmol > 0:
        if lidx == 0:
            if chain_id != ' ':
                # look up chain
                for mol_idx, mol_name in enumerate(gro.molecule_names):
                    if mol_name == 'W' or mol_name == 'WF':
                        #ignore solvent
                        continue
                    # Chain name ends with ID
                    if mol_name.endswith(chain_id):
                        break
                my_chain_id = chain_id
            else:
                mol_idx = imol
                my_chain_id = gro.molecule_names[mol_idx]
            print('#{}: '.format(my_chain_id),end='')

            body_pos[my_chain_id] = []
            body_type[my_chain_id] = []
            body_res[my_chain_id] = []
            body_resid[my_chain_id] = []
            body_charge[my_chain_id] = []
            body_bfact[my_chain_id] = []
            body_mass[my_chain_id] = []
            body_total_mass[my_chain_id] = 0.0

            groups[my_chain_id] = []

        n_rexcl = gro.molecules[mol_idx]['exclusion_range']

        natoms = len(gro.molecules[mol_idx]['atom_type'])

        chain_pos_local = []
        chain_type_local = []

        lvec = [pdb.lattice_vectors[0]/10.0, pdb.lattice_vectors[1]/10.0, pdb.lattice_vectors[2]/10.0]

        p = pdb.pos[atom_idx]
        r = pdb.residues[atom_idx]
        seq = pdb.res_seq[atom_idx]
        bfact = pdb.b_factors[atom_idx]

        # Angstrom to nm
        p = (p[0]/10.0, p[1]/10.0, p[2]/10.0)

        pwrap = p - np.dot((0.5,0.5,0.5),np.array(lvec))

        img = (0,0,0)
        # wrap multiple times
        for i in range(3):
            (pwrap,img) = data.boxdim(Lx=pdb.lx/10.0, Ly=pdb.ly/10.0, Lz=pdb.lz/10.0, xy=pdb.xy, xz=pdb.xz, yz=pdb.yz).wrap(pwrap,img)

        t = gro.molecules[mol_idx]['atom_type'][lidx]
#        charge = gro.molecules[mol_idx]['charge'][lidx]*math.sqrt(138.967/epsilon)
        charge = gro.molecules[mol_idx]['charge'][lidx]

        if len(gro.molecules[mol_idx]['mass']) > 0:
            m = gro.molecules[mol_idx]['mass'][lidx]
        else:
            for (mass, type_name) in zip(gro.type_masses, gro.type_names):
                if type_name == t:
                    m = mass

        siteno = gro.molecules[mol_idx]['atom_no'][lidx]
        if siteno in gro.molecules[mol_idx]['virtual_sites']:
            # virtual sites are not supported currently, make this a special (inactive) type
            t = 'inactive'
            has_virtual_sites = True

            # give them a large mass, so they don't fly around too much
            m = 1e8

        chain_type_local.append(t)

        xml_pos.append(pwrap)
        xml_type.append(t)
        xml_charge.append(charge)
        xml_image.append(img)
        xml_body.append(cur_body)
        xml_mass.append(m)

        body_pos[my_chain_id].append(p)
        body_type[my_chain_id].append(t)
        body_res[my_chain_id].append(r)
        body_resid[my_chain_id].append(seq)
        body_charge[my_chain_id].append(charge)
        body_bfact[my_chain_id].append(bfact)

        body_mass[my_chain_id].append(m)
        body_total_mass[my_chain_id] += m

        groups[my_chain_id].append(atom_idx)

        atom_idx+=1
        lidx += 1
        nmol -= 1

        if lidx == natoms:
            lidx = 0
            imol += 1
            cur_body = atom_idx
            chain_type[my_chain_id] = chain_type_local

            print('{} types,'.format(len(set(chain_type[my_chain_id]))), end =' ')

            for bb_atom in gro.molecules[mol_idx]['backbone_atoms']:
                backbone.append(bb_atom+chain_offset)

            nconstraint = 0
            for c, d in zip(gro.molecules[mol_idx]['constraints'], gro.molecules[mol_idx]['constraint_d']):
                c = (c[0] + chain_offset, c[1] + chain_offset, d)
                xml_constraint.append(c)
                nconstraint += 1

            nbond = 0
            for bond, bond_type in zip(gro.molecules[mol_idx]['bonds'], gro.molecules[mol_idx]['bond_types']):
                b = (bond[0]+chain_offset, bond[1]+chain_offset)
                xml_bond.append(b)
                xml_bond_type.append(bond_type)
                nbond += 1

            npair = 0
            for pair, pair_type in zip(gro.molecules[mol_idx]['pairs'], gro.molecules[mol_idx]['pair_types']):
                b = (pair[0]+chain_offset, pair[1]+chain_offset)
                xml_pair.append(b)
                xml_pair_type.append(pair_type)
                npair += 1

            nangle = 0
            for angle, angle_type in zip(gro.molecules[mol_idx]['angles'], gro.molecules[mol_idx]['angle_types']):
                a = (angle[0]+chain_offset, angle[1]+chain_offset, angle[2]+chain_offset)

                xml_angle.append(a)
                xml_angle_type.append(angle_type)
                nangle += 1

            ndihedral = 0
            for dihedral, dihedral_type in zip(gro.molecules[mol_idx]['dihedrals'], gro.molecules[mol_idx]['dihedral_types']):
                d = (dihedral[0]+chain_offset, dihedral[1]+chain_offset, dihedral[2]+chain_offset, dihedral[3]+chain_offset)

                xml_dihedral.append(d)
                xml_dihedral_type.append(dihedral_type)
                ndihedral += 1

            nimproper = 0
            for improper, improper_type in zip(gro.molecules[mol_idx]['impropers'], gro.molecules[mol_idx]['improper_types']):
                i = (improper[0]+chain_offset, improper[1]+chain_offset, improper[2]+chain_offset, improper[3]+chain_offset)

                xml_improper.append(i)
                xml_improper_type.append(improper_type)
                nimproper += 1

            chain_offset += natoms

            print('%d atoms, %d constraints, %d bonds, %d pairs, %d angles, %d dihedrals, %d impropers' % (natoms,nconstraint, nbond, npair, nangle,ndihedral,nimproper))

with open(xml_file,'w') as out:
    out.write('<?xml version="1.0" encoding="UTF-8"?>\n\
<hoomd_xml version="1.8">\n\
<configuration time_step="0" dimensions="3" natoms="%d">\n' % (len(xml_pos)))

    # angstrom to nm
    out.write('<box lx=%f ly=%f lz=%f xy=%f xz=%f yz=%f />\n' % (pdb.lx/10.0,pdb.ly/10.0, pdb.lz/10.0, pdb.xy, pdb.xz, pdb.yz))
    out.write('<position num=%d>\n' % len(xml_pos))
    for p in xml_pos:
        out.write('%f %f %f\n' % (p[0], p[1], p[2]))
    out.write('</position>\n')
    out.write('<type num=%d>\n' % len(xml_type))
    for t in xml_type:
        out.write('%s\n' % t)
    out.write('</type>\n')
    out.write('<image num=%d>\n'.format(len(xml_image)))
    for c in xml_image:
        out.write('{} {} {}\n'.format(c[0],c[1],c[2]))
    out.write('</image>\n')
    out.write('<mass num=%d>\n' % len(xml_mass))
    for m in xml_mass:
        out.write('%f\n' % m)
    out.write('</mass>\n')
    out.write('<charge num=%d>\n' % len(xml_charge))
    for c in xml_charge:
        out.write('%f\n' % c)
    out.write('</charge>\n')
    out.write('<constraint num={}>\n'.format(len(xml_constraint)))
    for c in xml_constraint:
        out.write('{} {} {:.5f}\n'.format(c[0],c[1],c[2]))
    out.write('</constraint>\n')
    out.write('<bond num=%d>\n' % len(xml_bond))
    for (bond_type,b) in zip(xml_bond_type, xml_bond):
        out.write('%s %d %d\n' % (bond_type, b[0], b[1]))
    out.write('</bond>\n')
    out.write('<pair num=%d>\n' % len(xml_pair))
    for (pair_type,b) in zip(xml_pair_type, xml_pair):
        out.write('%s %d %d\n' % (pair_type, b[0], b[1]))
    out.write('</pair>\n')
    out.write('<angle num=%d>\n' % len(xml_angle))
    for (angle_type,a) in zip(xml_angle_type, xml_angle):
        out.write('%s %d %d %d\n' % (angle_type, a[0], a[1], a[2]))
    out.write('</angle>\n')
    out.write('<dihedral num=%d>\n' % len(xml_dihedral))
    for (dihedral_type,d) in zip(xml_dihedral_type, xml_dihedral):
        out.write('%s %d %d %d %d\n' % (dihedral_type, d[0], d[1], d[2], d[3]))
    out.write('</dihedral>\n')
    out.write('<improper num=%d>\n' % len(xml_improper))
    for (improper_type,i) in zip(xml_improper_type, xml_improper):
        out.write('%s %d %d %d %d\n' % (improper_type, i[0], i[1], i[2], i[3]))
    out.write('</improper>\n')

    out.write('</configuration>\n')
    out.write('</hoomd_xml>\n')

# Output force field definition
def write_py(filename):
    if filename is not None:
        out = open(filename, 'w')
    else:
        out = sys.stdout

    # non-bonded
    out.write('# include this in your hoomd script after init.*() (import {})\n'.format(filename))
    out.write('from hoomd import *\n')
    out.write('from hoomd import md\n')
    out.write('import math\n')

    out.write('def nonbonded_coeff():\n')
    out.write('    # non-bonded\n')
    out.write('    util.quiet_status()\n')
    out.write('    coeff = md.pair.coeff()\n')
    for ((typei, typej), eps,sigma) in zip(gro.type_pairs,gro.eps,gro.sigma):
        out.write('    coeff.set(\'{}\',\'{}\', epsilon={:.3f}, sigma={:.3f})\n'.format(typei, typej, eps,sigma))

    if has_virtual_sites:
        for (typei, typej) in gro.type_pairs:
            if (typei == typej):
                # add inactive LJ coefficient
                out.write('    coeff.set(\'inactive\',\'{}\', epsilon=0.0, sigma=0.0, r_cut=False)\n'.format(typei))
        out.write('    coeff.set(\'inactive\',\'inactive\', epsilon=0.0, sigma=0.0, r_cut=False)\n')

    out.write('    util.unquiet_status()\n')
    out.write('    return coeff\n')

    # construct a diametr type dict for convenience
    # set diameter from LJ self-interaction sigma
    out.write('\ndiameter = dict()\n')
    for ((typei, typej), eps,sigma) in zip(gro.type_pairs,gro.eps,gro.sigma):
        if typei == typej:
            out.write('diameter[\'{}\'] = {:.3f}\n'.format(typei,sigma))

    if has_virtual_sites:
        out.write('diameter[\'inactive\'] = 0\n')

    out.write('\nbond_harmonic=None\n')
    out.write('\nangle_cos2=None\n')
    out.write('\nangle_harmonic=None\n')
    out.write('\ndihedral_harmonic=None\n')
    out.write('\ndimproper_harmonic=None\n')
    out.write('\ndistance=None\n')
    out.write('def intrachain(nlist,nb_cut):\n')
    # bonds
    out.write('    # exclusions\n')
    out.write('    excl_list = [')
    if n_rexcl >= 1:
        out.write('\'bond\', \'constraint\', \'body\'')
    if n_rexcl >= 2:
        out.write(', \'1-3\'')
    if n_rexcl >= 3:
        out.write(', \'1-4\'')
    out.write(']\n')
    out.write('    nlist.reset_exclusions(excl_list)\n\n')
    out.write('    # bonds\n')
    out.write('    global bond_harmonic\n')
    out.write('    bond_harmonic = md.bond.harmonic()\n')
    out.write('    util.quiet_status()\n')
    for (bond_type, r0, kappa) in zip(gro.bond_type_names, gro.bond_type_r0, gro.bond_type_kappa):
        out.write('    bond_harmonic.bond_coeff.set(\'%s\', r0=%f, k=%f)\n' % (bond_type, r0, kappa))
    out.write('    util.unquiet_status()\n')

    if npair > 0:
        out.write('    global special_pair_lj\n')
        out.write('    special_lj = md.special_pair.lj()\n')
        for (pair_type, epsilon, sigma) in zip(gro.pair_type_names, gro.pair_type_epsilon, gro.pair_type_sigma):
            out.write('    special_lj.pair_coeff.set(\'{}\', epsilon={:.5f}, sigma={:.5f}, r_cut=nb_cut)\n'.format(pair_type, epsilon, sigma))

    # angles
    has_cos2_angles = False
    has_harmonic_angles = False
    for angle_type in gro.angle_type_names:
        if angle_type.startswith('angle_cos2'):
            has_cos2_angles = True
        else:
            has_harmonic_angles = True

    if has_cos2_angles:
        out.write('\n    # angles\n')
        out.write('    global angle_cos2\n')
        out.write('    angle_cos2 = md.angle.cosinesq()\n')
    if has_harmonic_angles:
        out.write('\n    # angles\n')
        out.write('    global angle_harmonic\n')
        out.write('    angle_harmonic = md.angle.harmonic()\n')

    for (angle_type, theta0, kappa) in zip(gro.angle_type_names, gro.angle_type_theta0, gro.angle_type_kappa):

        if angle_type in xml_angle_type:
            if angle_type.startswith('angle_cos2'):
                out.write('    angle_cos2.angle_coeff.set(\'%s\',t0=%f, k=%f)\n' % (angle_type, math.pi/180.0*theta0, kappa))

                if has_harmonic_angles:
                    # set other angle coeff to zero
                    out.write('    angle_harmonic.angle_coeff.set(\'{}\', k=0.0, t0=0)\n'.format(angle_type))
            else:
                out.write('    angle_harmonic.angle_coeff.set(\'{}\', k={:.3f}, t0={:.3f})\n'.format(angle_type, kappa, math.pi/180.0*theta0))
                if has_cos2_angles:
                    # set other angle coeff to zero
                    out.write('    angle_cos2.angle_coeff.set(\'{}\', t0=0, k=0)\n'.format(angle_type))

    # dihedrals
    if len(xml_dihedral) > 0:
        dihedral_num_points = 100
        out.write('\n    # dihedrals\n')
        out.write('    global dihedral_harmonic\n')
        out.write('    def dihedral_shift(theta, kappa, theta_0,n):\n')
        out.write('        V = kappa * (1+math.cos(n*theta-theta_0))\n')
        out.write('        T = kappa*n*math.sin(n*theta-theta_0)\n')
        out.write('        return (V, T)\n')
        out.write('    dihedral_harmonic = md.dihedral.table(width=%d)\n' % dihedral_num_points)
        for (dihedral_type, theta0, kappa, n) in zip(gro.dihedral_type_names, gro.dihedral_type_theta0, gro.dihedral_type_kappa, gro.dihedral_type_n):
            if dihedral_type in xml_dihedral_type:
                out.write('    dihedral_harmonic.dihedral_coeff.set(\'%s\', coeff=dict(theta_0=%f, kappa=%f, n=%f), func=dihedral_shift)\n' % (dihedral_type, math.pi/180.0*theta0, kappa, n))

    # impropers
    if len(xml_improper) > 0:
        out.write('\n    # impropers\n')
        out.write('\n    global improper_harmonic\n')
        out.write('    improper_harmonic = md.improper.harmonic()\n')
        for (improper_type, theta0, kappa) in zip(gro.improper_type_names, gro.improper_type_theta0, gro.improper_type_kappa):
            out.write('    improper_harmonic.improper_coeff.set(\'%s\', chi=%f, k=%f)\n' % (improper_type, math.pi/180.0*theta0,kappa))

    # constraints
    if len(xml_constraint) > 0:
        out.write('\n    #enable constraints\n')
        out.write('\n    global distance\n')
        out.write('    distance = md.constrain.distance()\n')
        out.write('    distance.set_params(rel_tol=0.005)\n')

    out.write('\n#Rigid bodies\n')
    out.write('from collections import OrderedDict\n')
    out.write('body_pos = OrderedDict()\n')
    out.write('body_type = OrderedDict()\n')
    out.write('body_res = OrderedDict()\n')
    out.write('body_resid = OrderedDict()\n')
    out.write('body_charge = OrderedDict()\n')
    out.write('body_bfact = OrderedDict()\n')
    out.write('body_mass = OrderedDict()\n')
    out.write('body_total_mass = OrderedDict()\n')
    for chain_id in body_pos:
        out.write('body_pos[\'{}\'] = [{}]\n'.format(chain_id,','.join(['({:.5f}, {:5f}, {:.5f})'.format(t[0],t[1],t[2]) \
            for t in body_pos[chain_id]])))
        out.write('body_type[\'{}\'] = [{}]\n'.format(chain_id,','.join(['\'{}\''.format(t) for t in body_type[chain_id]])))
        out.write('body_res[\'{}\'] = [{}]\n'.format(chain_id,','.join(['\'{}\''.format(t) for t in body_res[chain_id]])))
        out.write('body_resid[\'{}\'] = [{}]\n'.format(chain_id,','.join(['{}'.format(t) for t in body_resid[chain_id]])))
        out.write('body_charge[\'{}\'] = [{}]\n'.format(chain_id,','.join(['{:3f}'.format(c) for c in body_charge[chain_id]])))
        out.write('body_bfact[\'{}\'] = [{}]\n'.format(chain_id,','.join(['{:3f}'.format(c) for c in body_bfact[chain_id]])))
        out.write('body_mass[\'{}\'] = [{}]\n'.format(chain_id,','.join(['{:3f}'.format(m) for m in body_mass[chain_id]])))
        out.write('body_total_mass[\'{}\'] = {:.5f}\n'.format(chain_id,body_total_mass[chain_id]))
        out.write('\n')

    out.close()

# write the python script
write_py(ff_file)

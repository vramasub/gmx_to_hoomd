import sys
import math
import os
import ntpath

itp_path = '/Users/jglaser/local/share/gromacs/top'

class gromacs:
    def __init__(self):
        # atom types
        self.type_masses = dict()
        self.type_charges = dict()

        # LJ parameters
        self.type_pairs = []
        self.eps = []
        self.sigma = []

        # chains in system
        self.chains = []

        # molecule types
        self.molecule_names = []
        self.molecules = []

        # bond types
        self.bond_type_names = []
        self.bond_type_r0 = []
        self.bond_type_kappa = []
        self.pair_type_names = []
        self.pair_type_epsilon = []
        self.pair_type_sigma = []

        # angle types
        self.angle_type_names = []
        self.angle_type_theta0 = []
        self.angle_type_kappa = []

        # dihedral types
        self.dihedral_type_names = []
        self.dihedral_type_theta0 = []
        self.dihedral_type_kappa = []
        self.dihedral_type_n = []

        # improper types
        self.improper_type_names = []
        self.improper_type_theta0 = []
        self.improper_type_kappa = []

        self.defines = dict()

        # read in position restraints to find backbone atoms
        self.defines['POSRES'] = True

        # for now use flexible water (no constraints)
        self.defines['FLEXIBLE'] = True

    def read_topology(self,filename):
        import os
#        self.read_itp(filename,os.path.dirname(os.path.realpath(__file__)))
        self.read_itp(filename,os.getcwd())

        # apply combination rule
        for typei,typej in self.type_pairs:
            if typei == typej == 'H':
                print('Here')
        for i,((typei,typej), eps,sigma) in enumerate(zip(self.type_pairs,self.eps,self.sigma)):
            if eps == 0.0 and sigma == 0.0:
                if self.combination_rule == 1:
                    found = False
                    for (ti,tj),eps,sigma in zip(self.type_pairs,self.eps,self.sigma):
                        if ti == typei and tj == typei:
                            found = True
                            c6i = 4*eps*sigma**6.0
                            c12i = 4*eps*sigma**12.0
                            break
                    if found:
                        found = False
                        for (ti,tj),eps,sigma in zip(self.type_pairs,self.eps,self.sigma):
                            if ti == typej and tj == typej:
                                found = True
                                c6j = 4*eps*sigma**6.0
                                c12j = 4*eps*sigma**12.0
                                break
                        if found:
                            c6 = math.sqrt(c6i*c6j)
                            c12 = math.sqrt(c12i*c12j)

                            if c6 != 0.0 and c12 != 0.0:
                                eps = math.pow(float(c6),2.0)/4.0/float(c12)
                                sigma = math.pow(float(c12)/float(c6),1./6.)
                            else:
                                eps = 0.0
                                sigma = 0.0

                            self.eps[i] = eps
                            self.sigma[i] = sigma
                        else:
                            raise RuntimeError('Could not apply combination rule for {} {}'.format(typei,typej))
                else:
                    raise RuntimeError("Don't know how to apply combination rule for {} {}".format(typei,typej))

    def read_itp(self,filename,cur_dir):
        # #ifdef stack
        ignore=[]

        with open(cur_dir+'/'+filename,'r') as itp:
            print('#Reading {}'.format(filename))
            cur_section = ''
            cur_molecule = ''
            for l in itp.readlines():
                if len(l)>0 and l[0] == ';':
                    continue

                tokens = l.lstrip().split(';',1)[0].split()

                if len(tokens)== 0 or tokens[0][0] == ';':
                    continue

                if tokens[0] == "#endif":
                    # no nested ifdefs
                    ignore.pop()
                    continue

                if tokens[0] == "#else":
                    # no nested ifdefs
                    ignore[-1] = not ignore[-1]
                    continue

                if len(ignore) > 1 and ignore[-1]:
                    continue

                if tokens[0] == "#ifdef":
                    if tokens[1] not in self.defines:
                        ignore.append(True)
                    else:
                        ignore.append(False)
                    continue

                if tokens[0]=="#ifndef":
                    if tokens[1] in self.defines:
                        ignore.append(True)
                    else:
                        ignore.append(False)
                    continue

                if tokens[0] == "#include":
                    incfile = tokens[1].replace('"','')

                    try:
                        self.read_itp(incfile,ntpath.dirname(cur_dir+'/'+filename))
                    except Exception:
                        self.read_itp(incfile,ntpath.dirname(itp_path+'/'+filename))
                    continue;

                if tokens[0] == '#define':
                    if len(tokens) > 2:
                        self.defines[tokens[1]] = tokens[2:]
                    else:
                        self.defines[tokens[1]] = True
                    continue

                # new section?
                if tokens[0] == '[':
                    if tokens[2] != ']':
                        raise RuntimeError('Invalid section')

                    cur_section = tokens[1]
                    continue

                if cur_section == 'defaults':
                    if len(tokens) >= 5:
                        (nbfunc, cr, gen_pairs, fudge_lj, fudge_qq) = tokens[0:5]
                    else:
                        (nbfunc,cr) = tokens[0:2]
                    self.combination_rule = int(cr)

                if cur_section == 'atomtypes':
                    if len(tokens) >= 7:
                        (type_name, atnum, mass, charge) = tokens[0:4]
                    else:
                        (type_name, mass, charge) = tokens[0:3]

                    self.type_masses[type_name] = float(mass)
                    self.type_charges[type_name] = float(charge)

                    # special handling of OPLS force field with tabs in bond types
                    if len(tokens) == 8:
                        tokens = (tokens[0], tokens[1]+tokens[2], tokens[3],tokens[4],tokens[5],tokens[6],tokens[7])
                    if len(tokens) == 7:
                        # add self interaction
                        typei = typej= type_name
                        self.type_pairs.append((typei,typej))
                        if self.combination_rule == 1:
                            (c6,c12) = tokens[5:7]

                            # convert c6 c12 to epsilon sigma
                            if float(c12) != 0.0 and float(c6) != 0.0:
                                self.eps.append(math.pow(float(c6),2.0)/4.0/float(c12))
                                self.sigma.append(math.pow(float(c12)/float(c6),1./6.))
                            else:
                                self.eps.append(0)
                                self.sigma.append(0)
                        else:
                            self.sigma.append(float(tokens[5]))
                            self.eps.append(float(tokens[6]))

                    if len(tokens) == 6:
                        # add self interaction
                        typei = typej= type_name
                        (sigma,eps) = tokens[4:6]
                        sigma = float(sigma)
                        eps = float(eps)
                        if sigma != 0.0:
                            self.type_pairs.append((typei,typej))

                            self.eps.append(eps)
                            self.sigma.append(sigma)


                if cur_section == 'nonbond_params':
                    (typei, typej, funda, c6, c12) = tokens[0:5]

                    self.type_pairs.append((typei,typej))

                    if self.combination_rule == 1:
                        # convert c6 c12 to epsilon sigma
                        if float(c12) != 0.0 and float(c6) != 0.0:
                            self.eps.append(math.pow(float(c6),2.0)/4.0/float(c12))
                            self.sigma.append(math.pow(float(c12)/float(c6),1./6.))
                        else:
                            self.eps.append(0)
                            self.sigma.append(0)
                    else:
                        self.eps.append(float(c12))
                        self.sigma.append(float(c6))

                if cur_section == 'moleculetype':
                    name = tokens[0]
                    excl_range = int(tokens[1])
                    cur_molecule = name
                    self.molecule_names.append(name)
                    mol={'atom_no': [], 'atom_type': [], 'residue_no': [], 'residue_name': [], 'atom_name': [], 'charge': [], 'mass': [],
                         'bonds': [], 'bond_types': [], 'pairs': [], 'pair_types': [], 'angles': [], 'angle_types': [], 'dihedrals': [], 'dihedral_types': [],
                         'impropers': [], 'improper_types': [], 'backbone_atoms': [], 'constraints': [], 'constraint_d': [],
                         'virtual_sites': [], 'exclusion_range': excl_range}
                    self.molecules.append(mol)

                if cur_section == 'molecules':
                    name = tokens[0]
                    count = int(tokens[1])
                    for i in range(count):
                        self.chains.append(name)

                if cur_section == 'virtual_sitesn':
                    # we currently don't have support for virtual sites, so just mark them
                    siteno = int(tokens[0])
                    self.molecules[-1]['virtual_sites'].append(siteno)

                if cur_section == 'atoms':
                    (atom_no, atom_type, residue_no, residue_name, atom_name, cg_nr, charge) = tokens[0:7]
                    self.molecules[-1]['atom_no'].append(int(atom_no))
                    self.molecules[-1]['atom_type'].append(atom_type)
                    self.molecules[-1]['residue_no'].append(int(residue_no))
                    self.molecules[-1]['residue_name'].append(residue_name)
                    self.molecules[-1]['atom_name'].append(atom_name)
                    self.molecules[-1]['charge'].append(float(charge))
                    if len(tokens) == 8:
                        mass = tokens[7]
                        self.molecules[-1]['mass'].append(float(mass))
                    else:
                        self.molecules[-1]['mass'].append(self.type_masses[atom_type])

                if cur_section == 'bondtypes':
                    if len(tokens) >= 5:
                        (type_a, type_b, func, b0, k) = tokens[0:6]
                    elif len(tokens) == 4:
                        (type_a, type_b, func) = tokens[0:3]
                        (b0, k) = self.defines[tokens[3]]

                    # create new type
                    self.bond_type_names.append('bond_{}_{}'.format(type_a,type_b))
                    self.bond_type_r0.append(float(b0))
                    self.bond_type_kappa.append(float(k))

                if cur_section == 'bonds':
                    if len(tokens) >= 5:
                        (a, b, funct, r0, kappa) = tokens[0:5]
                    elif len(tokens) == 4:
                        a = tokens[0]
                        b = tokens[1]
                        funct = tokens[2]
                        (r0, kappa) = self.defines[tokens[3]]
                    elif len(tokens) == 3:
                        a = tokens[0]
                        b = tokens[1]
                        type_a = self.molecules[-1]['atom_type'][int(a)-1]
                        type_b = self.molecules[-1]['atom_type'][int(b)-1]
                        bond_type = 'bond_{}_{}'.format(type_a,type_b)
                        self.molecules[-1]['bonds'].append((int(a)-1,int(b)-1))
                        self.molecules[-1]['bond_types'].append(bond_type)

                    if len(tokens) >= 4:
                        funct = int(funct)
                        kappa = float(kappa)
                        r0 = float(r0)

                        if funct == 2:
                            # convert GROMOS 4th power to harmonic
                            kappa = 2*kappa*r0*r0

                        if (float(r0), float(kappa)) not in zip(self.bond_type_r0, self.bond_type_kappa):
                            # create new type
                            self.bond_type_names.append('bond'+str(len(self.bond_type_names)))
                            self.bond_type_r0.append(float(r0))
                            self.bond_type_kappa.append(float(kappa))
                            type_idx = len(self.bond_type_names)-1
                        else:
                            for type_idx,s in enumerate(zip(self.bond_type_r0, self.bond_type_kappa)):
                                if s == (float(r0),float(kappa)):
                                    break

                        self.molecules[-1]['bonds'].append((int(a)-1,int(b)-1))
                        self.molecules[-1]['bond_types'].append(self.bond_type_names[type_idx])

                if cur_section == 'pairs':
                    a = tokens[0]
                    b = tokens[1]
                    func = int(tokens[2])
                    if func != 1:
                        raise RuntimeError('Unknown pairs type')

                    type_a = self.molecules[-1]['atom_type'][int(a)-1]
                    type_b = self.molecules[-1]['atom_type'][int(b)-1]

                    if type_a < type_b:
                        type_a, type_b = type_b, type_a

                    pair_type = 'pair_{}_{}'.format(type_a,type_b)
                    self.molecules[-1]['pairs'].append((int(a)-1,int(b)-1))
                    self.molecules[-1]['pair_types'].append(pair_type)

                if cur_section == 'pairtypes':
                    (type_a, type_b, func, c6, c12) = tokens[0:6]

                    # convert c6 c12 to epsilon sigma
                    if float(c12) != 0.0 and float(c6) != 0.0:
                        eps = math.pow(float(c6),2.0)/4.0/float(c12)
                        sigma = math.pow(float(c12)/float(c6),1./6.)
                    else:
                        eps = sigma = 0

                    if type_a < type_b:
                        type_a, type_b = type_b, type_a

                    # create new type
                    self.pair_type_names.append('pair_{}_{}'.format(type_a,type_b))
                    self.pair_type_epsilon.append(eps)
                    self.pair_type_sigma.append(sigma)

                if cur_section == 'constraints':
                    (a, b, funct, d) = tokens[0:4]

                    d = float(d)

                    if int(funct) != 1:
                        raise RuntimeError('Unknown constraint type')

                    self.molecules[-1]['constraints'].append((int(a)-1,int(b)-1))
                    self.molecules[-1]['constraint_d'].append(d)

                if cur_section == 'angletypes':
                    if len(tokens) >= 6:
                        (type_a, type_b, type_c, func, th0, cth) = tokens[0:6]
                    elif len(tokens) == 5:
                        (type_a, type_b, type_c, func) = tokens[0:4]
                        (th0, cth) = self.defines[tokens[4]]

                    (type_a, type_b, type_c) = sorted((type_a, type_b, type_c))
                    # create new type
                    self.angle_type_names.append('angle_{}_{}_{}'.format(type_a,type_b,type_c))
                    self.angle_type_theta0.append(float(th0))
                    self.angle_type_kappa.append(float(cth))

                if cur_section == 'angles':
                    if len(tokens)>=6:
                        (a, b, c, funct, theta0, kappa) = tokens[0:6]
                    else:
                        a = tokens[0]
                        b = tokens[1]
                        c = tokens[2]
                        funct = tokens[3]
                        if len(tokens) == 5:
                            (theta0, kappa) = self.defines[tokens[4]]

                    funct = int(funct)
                    if funct == 1:
                        # look up in particle types
                        type_a = self.molecules[-1]['atom_type'][int(a)-1]
                        type_b = self.molecules[-1]['atom_type'][int(b)-1]
                        type_c = self.molecules[-1]['atom_type'][int(c)-1]
                        (type_a, type_b, type_c) = sorted((type_a, type_b, type_c))
                        angle_type = 'angle_{}_{}_{}'.format(type_a,type_b,type_c)
                        self.molecules[-1]['angles'].append((int(a)-1,int(b)-1,int(c)-1))
                        self.molecules[-1]['angle_types'].append(angle_type)
                    elif funct == 2:
                        # cosine-angle potential
                    #if funct == 2:
                    #    # convert GROMOS96 angle to harmonic
                    #    theta0 = float(theta0)
                    #    kappa = kappa * math.sin(theta0) * math.sin(theta0)

                        if (float(theta0), float(kappa)) not in zip(self.angle_type_theta0, self.angle_type_kappa):
                            # create new type
                            self.angle_type_names.append('angle_cos2_'+str(len(self.angle_type_names)))
                            self.angle_type_theta0.append(float(theta0))
                            self.angle_type_kappa.append(float(kappa))
                            type_idx = len(self.angle_type_names)-1
                        else:
                            for type_idx,s in enumerate(zip(self.angle_type_theta0, self.angle_type_kappa)):
                                if s == (float(theta0),float(kappa)):
                                    break

                        self.molecules[-1]['angles'].append((int(a)-1,int(b)-1,int(c)-1))
                        self.molecules[-1]['angle_types'].append(self.angle_type_names[type_idx])

                if cur_section == 'dihedraltypes':
                    if len(tokens)>=8:
                        (type_a, type_b, type_c, type_d, func, phase, k, pn) = tokens[0:8]
                    elif len(tokens) == 4:
                        (type_c, type_d, func) = tokens[0:3]
                        type_a = type_b = '*'
                        (phase, k, pn) = self.defines[tokens[3]]

                    # create new type
                    self.dihedral_type_names.append('dihedral_{}_{}_{}_{}'.format(type_a,type_b,type_c,type_d))
                    self.dihedral_type_theta0.append(float(phase))
                    self.dihedral_type_kappa.append(float(k))
                    self.dihedral_type_n.append(int(pn))

                if cur_section == 'dihedrals':
                    if len(tokens)>=7:
                        f = int(tokens[4])
                        if f == 1:
                            (a, b, c, d, funct, theta0, kappa, n) = tokens[0:8]
                        elif f==2:
                            (a, b, c, d, funct, theta0, kappa) = tokens[0:7]
                    else:
                        a = tokens[0]
                        b = tokens[1]
                        c = tokens[2]
                        d = tokens[3]
                        funct = tokens[4]

                        if int(funct) == 1:
                            (theta0, kappa,n) = self.defines[tokens[5]]
                        elif int(funct) == 2:
                            (theta0, kappa) = self.defines[tokens[5]]

                    f = int(funct)
                    if f == 9 or f == 4:
                        # look up in dihedral types
                        type_a = self.molecules[-1]['atom_type'][int(a)-1]
                        type_b = self.molecules[-1]['atom_type'][int(b)-1]
                        type_c = self.molecules[-1]['atom_type'][int(c)-1]
                        type_d = self.molecules[-1]['atom_type'][int(a)-1]
                        dihedral_type = 'dihedral_{}_{}_{}_{}'.format(type_a,type_b,type_c,type_d)
                        self.molecules[-1]['dihedrals'].append((int(a)-1,int(b)-1,int(c)-1,int(d)-1))
                        self.molecules[-1]['dihedral_types'].append(dihedral_type)
                    elif f == 1:
                        if (float(theta0), float(kappa), int(n)) not in zip(self.dihedral_type_theta0, self.dihedral_type_kappa, self.dihedral_type_n):
                            # create new type
                            self.dihedral_type_names.append('dihedral'+str(len(self.dihedral_type_names)))
                            self.dihedral_type_theta0.append(float(theta0))
                            self.dihedral_type_kappa.append(float(kappa))
                            self.dihedral_type_n.append(int(n))
                            type_idx = len(self.dihedral_type_names)-1
                        else:
                            for type_idx,s in enumerate(zip(self.dihedral_type_theta0, self.dihedral_type_kappa, self.dihedral_type_n)):
                                if s == (float(theta0),float(kappa),int(n)):
                                    break

                        self.molecules[-1]['dihedrals'].append((int(a)-1,int(b)-1,int(c)-1,int(d)-1))
                        self.molecules[-1]['dihedral_types'].append(self.dihedral_type_names[type_idx])
                    elif f == 2:
                        if (float(theta0), float(kappa)) not in zip(self.improper_type_theta0, self.improper_type_kappa):
                            # create new type
                            self.improper_type_names.append('improper'+str(len(self.improper_type_names)))
                            self.improper_type_theta0.append(float(theta0))
                            self.improper_type_kappa.append(float(kappa))
                            type_idx = len(self.improper_type_names)-1
                        else:
                            for type_idx,s in enumerate(zip(self.improper_type_theta0, self.improper_type_kappa)):
                                if s == (float(theta0),float(kappa)):
                                    break

                        self.molecules[-1]['impropers'].append((int(a)-1,int(b)-1,int(c)-1,int(d)-1))
                        self.molecules[-1]['improper_types'].append(self.improper_type_names[type_idx])
                    else:
                        print('Unrecognized dihedral type %d' % f)
                        raise RuntimeError('Error reading topology')

                if cur_section == 'position_restraints':
                    atom_id = int(tokens[0])-1
                    self.molecules[-1]['backbone_atoms'].append(atom_id)
